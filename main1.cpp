#include <iostream>
using namespace std;
class Massiv {
public:
    int n, m, min, sum, sort;
    int **mass, *stolbecSort;

    int setVvod() {
        cout << "Введите N" << endl;
        cin >> n;
        cout << "Введите M" << endl;
        cin >> m;

        mass = new int *[n];
        for (int i = 0; i < n; i++) {
            mass[i] = new int[m];
            for (int j = 0; j < m; j++)
                mass[i][j] = -9 + rand() % 18;
            //cin >> mass[i][j];
        }
        return 1;
    }

    void getVivod() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++)
                cout << mass[i][j] << "  ";
            cout << endl;
        }
    }

    void getMin() {
        for (int i = 0; i < n; i++) {
            min = mass[i][0];
            for (int j = 0; j < m; j++)
                if (min > mass[i][j])
                    min = mass[i][j];
            cout << endl
                 << min;
        }
    }

    void getChet() {
        sum = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if ((i + j) % 2 == 0)
                    sum += mass[i][j];
            }
        }
        cout << endl
             << sum;
    }

    void *getStolbec(int stolbec) {
        int g = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (stolbec == j) {
                    stolbecSort[g] = mass[i][j];
                    g++;
                }
            }
        }
    }

    // Вписываем столбец обратно
    void returnStolbec(int stolbec) {
        for (int g = 0; g < n; g++) {
            for (int j = 0; j < n; j++)
                if (stolbec == j) {
                    mass[g][j] = stolbecSort[g];
                }
        }
    }

    void sortVstav() {
        stolbecSort = new int[n];// выделение памяти под массив
        for (int stolbec = 0; stolbec < n; stolbec++) {
            getStolbec(stolbec);// Выписываем нужный столбец

            for (int i = 1; i < n; i++) {
                for (int j = i; j > 0 && stolbecSort[j - 1] > stolbecSort[j]; j--) {
                    int tmp = stolbecSort[j - 1];
                    stolbecSort[j - 1] = stolbecSort[j];
                    stolbecSort[j] = tmp;
                }
            }

            returnStolbec(stolbec);// возмращаем столбец  на место
        }
    }
};


int main() {
    Massiv array;
    array.setVvod();
    array.getVivod();
    array.getMin();
    array.getChet();
    array.sortVstav();
    array.getVivod();
    return 0;
}
