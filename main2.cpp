#include <iostream>

using namespace std;

class Massiv {
public:
    int n, m, max, sort, k;
    double sum;
    int **mass, *stolbecSort;

    int setVvod() {
        cout << "Введите N" << endl;
        cin >> n;
        cout << "Введите M" << endl;
        cin >> m;

        mass = new int *[n];
        for (int i = 0; i < n; i++) {
            mass[i] = new int[m];
            for (int j = 0; j < m; j++)
                mass[i][j] = -9 + rand() % 18;
            //cin >> mass[i][j];
        }
        return 1;
    }
    void getVivod() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++)
                cout << mass[i][j] << "  ";
            cout << endl;
        }
    }
    void getMax() {
        for (int i = 0; i < n; i++) {
            max = mass[i][0];
            for (int j = 0; j < m; j++)
                if (max < mass[i][j])
                    max = mass[i][j];
            cout << endl
                 << max;
        }
        cout << endl;
    }
    void getSrArifm() {
        k = 0;
        sum = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (mass[i][j] < 0) {
                    sum += mass[i][j];
                    k++;
                }
            }
        }
        sum = sum / k;
        cout << endl
             << sum;
    }

    void *getStolbec(int stolbec) {
        int g = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (stolbec == j) {
                    stolbecSort[g] = mass[i][j];
                    g++;
                }
            }
        }
    }

    // Вписываем столбец обратно
    void returnStolbec(int stolbec) {
        for (int g = 0; g < n; g++) {
            for (int j = 0; j < n; j++)
                if (stolbec == j) {
                    mass[g][j] = stolbecSort[g];
                }
        }
    }


    //ф-ция для обмена значений ячеек
    void swapEl(int *arr, int i) {
        int buff;
        buff = arr[i];
        arr[i] = arr[i - 1];
        arr[i - 1] = buff;
    }

    //ф-ция "шейкер"-сортировки
    void sortShake() {
        stolbecSort = new int[n];// выделение памяти под массив
        for (int stolbec = 0; stolbec < n; stolbec++) {
            getStolbec(stolbec);// Выписываем нужный столбец

            int leftMark = 1;
            int rightMark = n - 1;
            while (leftMark <= rightMark) {
                for (int i = rightMark; i >= leftMark; i--)
                    if (stolbecSort[i - 1] > stolbecSort[i]) swapEl(stolbecSort, i);
                leftMark++;

                for (int i = leftMark; i <= rightMark; i++)
                    if (stolbecSort[i - 1] > stolbecSort[i]) swapEl(stolbecSort, i);
                rightMark--;

            }
            returnStolbec(stolbec);// возмращаем столбец  на место
        }
    }
};


int main() {
    Massiv array;
    array.setVvod();
    array.getVivod();
    array.getMax();
    array.getSrArifm();
    array.sortShake();
    array.getVivod();
    return 0;
}
