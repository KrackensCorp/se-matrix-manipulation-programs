#include <algorithm>
#include <iostream>;

using namespace std;

class Massiv {
public:
    int n, m, max, sort, k;
    double sum;
    int **mass, *StringSort;

    int setVvod() {
        cout << "Введите N: " << endl;
        cin >> n;
        cout << "Введите M: " << endl;
        cin >> m;

        mass = new int *[n];
        for (int i = 0; i < n; i++) {
            mass[i] = new int[m];
            for (int j = 0; j < m; j++)
                mass[i][j] = -9 + rand() % 18;
            //cin >> mass[i][j];
        }
        return 1;
    }

    void getVivod() {
        cout << endl;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++)
                cout << mass[i][j] << "  ";
            cout << endl;
        }
    }

    void getMax() {
        for (int j = 0; j < m; j++) {
            max = mass[0][j];
            for (int i = 0; i < n; i++)
                if (max < mass[i][j])
                    max = mass[i][j];
            cout << endl
                 << max;
        }
    }

    void getZamena() {
        for (int j = 0; j < m; j++) {
            max = mass[0][j];
            for (int i = 0; i < n; i++)
                if (max < mass[i][j])
                    max = mass[i][j];
            for (int i = 0; i < n; i++)
                if (mass[i][j] < 0)
                    mass[i][j] = max;
        }
    }


    void getString(int String) {
        int g = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (String == i) {
                    StringSort[g] = mass[i][j];
                    g++;
                }
            }
        }
    }

    // Вписываем строку обратно
    void returnString(int String) {
        for (int g = 0; g < n; g++) {
            for (int j = 0; j < n; j++)
                if (String == j) {
                    mass[j][g] = StringSort[g];
                }
        }
    }

    void sortVibor() {
        StringSort = new int[n];// выделение памяти под массив
        for (int String = 0; String < n; String++) {
            getString(String);// Выписываем нужный строку

            int length = n;
            std::sort(StringSort, StringSort + length);

            returnString(String);// возмращаем строку  на место
        }
    }
};


int main() {
    Massiv array;
    array.setVvod();
    array.getVivod();
    //array.getMax();
    //array.getZamena();
    array.sortVibor();
    array.getVivod();
    return 0;
}
