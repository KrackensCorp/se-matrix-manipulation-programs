#include <iostream>

using namespace std;

class Massiv {
public:
    int n, m, min, sort, k;
    double sum;
    int **mass, *StringSort;

    int setVvod() {
        cout << "Введите N";
        cin >> n;
        cout << "Введите M";
        cin >> m;

        mass = new int *[n];
        for (int i = 0; i < n; i++) {
            mass[i] = new int[m];
            for (int j = 0; j < m; j++)
                mass[i][j] = -9 + rand() % 18;
            //cin >> mass[i][j];
        }
        return 1;
    }
    void getVivod() {
        cout << endl;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++)
                cout << mass[i][j] << "  ";
            cout << endl;
        }
    }
    void getMin() {

        for (int j = 0; j < m; j++) {
            min = mass[0][j];
            for (int i = 0; i < n; i++)
                if (min > mass[i][j])
                    min = mass[i][j];
            cout << endl
                 << min;
        }
    }
    void getZamena() {
        for (int j = 0; j < m; j++) {
            min = mass[0][j];
            for (int i = 0; i < n; i++)
                if (min > mass[i][j])
                    min = mass[i][j];
            for (int i = 0; i < n; i++)
                if (mass[i][j] % 2 != 0)
                    mass[i][j] += min;
        }
    }

    void getString(int String) {
        int g = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (String == i) {
                    StringSort[g] = mass[i][j];
                    g++;
                }
            }
        }
    }

    // Вписываем строку обратно
    void returnString(int String) {
        for (int g = 0; g < n; g++) {
            for (int j = 0; j < n; j++)
                if (String == j) {
                    mass[j][g] = StringSort[g];
                }
        }
    }

    // Функция сортировки Шелла
    void shellSort() {
        StringSort = new int[n];// выделение памяти под массив
        for (int String = 0; String < n; String++) {
            getString(String);// Выписываем нужный строку

            int d = n / 2;//Длина промежутков между элементами
            while (d > 0) {
                for (int i = 0; i < n - d; i++) {
                    int j = i;
                    while (j >= 0 && stolbecSort[j] > stolbecSort[j + d]) {
                        int temp = stolbecSort[j];
                        stolbecSort[j] = stolbecSort[j + d];
                        stolbecSort[j + d] = temp;
                        j--;
                    }
                }
                d = d / 2;
            }
            returnString(String);// возмращаем строку  на место
        }
    }
};


int main() {
    Massiv array;
    array.setVvod();
    array.getVivod();
    array.getMin();
    array.getZamena();
    array.shellSort();
    array.getVivod();
    return 0;
}
