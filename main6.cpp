#include <iostream>

using namespace std;

class Massiv {
public:
    int n, m, min, sort;
    double sum, k, glav, slav;
    int **mass, *stolbecSort;

    int setVvod() {
        cout << "Введите N" << endl;
        cin >> n;

        mass = new int *[n];
        for (int i = 0; i < n; i++) {
            mass[i] = new int[n];
            for (int j = 0; j < n; j++)
                mass[i][j] = -9 + rand() % 18;
            //cin >> mass[i][j];
        }
        return 1;
    }

    void getVivod() {
        cout << endl;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++)
                cout << mass[i][j] << "  ";
            cout << endl;
        }
    }

    void getSrArifm() {

        for (int i = 0; i < n; i++) {
            k = 0;
            for (int j = 0; j < n; j++)
                k += mass[i][j];
            k = k / n;
            cout << endl
                 << k;
        }
    }

    void getSrav() {
        glav = 0;
        slav = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j)
                    glav += mass[i][j];
                if (n - 1 == i + j)
                    slav += mass[i][j];
            }
        }

        sum = (glav / n) + (slav / n);
        cout << endl
             << sum;
    }

    void *getStolbec(int stolbec) {
        int g = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (stolbec == j) {
                    stolbecSort[g] = mass[i][j];
                    g++;
                }
            }
        }
    }

    // Вписываем столбец обратно
    void returnStolbec(int stolbec) {
        for (int g = 0; g < n; g++) {
            for (int j = 0; j < n; j++)
                if (stolbec == j) {
                    mass[g][j] = stolbecSort[g];
                }
        }
    }

    void sortBuble() {
        stolbecSort = new int[n];// выделение памяти под массив
        for (int stolbec = 0; stolbec < n; stolbec++) {
            getStolbec(stolbec);// Выписываем нужный столбец
            int temp;           // временная переменная для обмена элементов местами
            // Сортировка массива пузырьком
            for (int i = 0; i < n - 1; i++) {
                for (int j = 0; j < n - i - 1; j++) {
                    if (stolbecSort[j] > stolbecSort[j + 1]) {
                        // меняем элементы местами
                        temp = stolbecSort[j];
                        stolbecSort[j] = stolbecSort[j + 1];
                        stolbecSort[j + 1] = temp;
                    }
                }
            }
            returnStolbec(stolbec);// возмращаем столбец  на место
        }
    }
};


int main() {
    Massiv array;
    array.setVvod();
    array.getVivod();
    array.getSrArifm();
    array.getSrav();
    array.sortBuble();
    array.getVivod();
    return 0;
}
