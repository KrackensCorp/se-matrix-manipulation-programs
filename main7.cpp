#include <iostream>
#include <valarray>

using namespace std;
int n, m, min, sum, sort;
int **mass, *stolbecSort;

//  Среднее арифметическое в массиве
double arithmeticalMean() {
    double sumArr = 0;
    int counter = 0;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            sumArr += mass[i][j];
            counter++;
        }
    }
    return (sumArr / counter);
}

// Проверка на простое число
bool simpleNumberOrNot(int element) {
    if (element > 1) {
        for (int i = 2; i < element; i++)
            if (element % i == 0)
                return false;
        return true;
    } else {
        return false;
    }
}

// Подсчет простых чисел в массиве
int counterSimpleNumber() {
    bool trap = false;
    int simpleNumbers = 0;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            trap = simpleNumberOrNot(mass[i][j]);
            if (trap) {
                simpleNumbers++;
            }
            trap = false;
        }
    }
}

// Быстрая сортировка одномерного Массива
int *quickSort(int *a, int left, int right) {
    int pivot;         // разрешающий элемент
    int l_hold = left; //левая граница
    int r_hold = right;// правая граница
    pivot = a[left];
    while (left < right)// пока границы не сомкнутся
    {
        while ((a[right] >= pivot) && (left < right))
            right--;      // сдвигаем правую границу пока элемент [right] больше [pivot]
        if (left != right)// если границы не сомкнулись
        {
            a[left] = a[right];// перемещаем элемент [right] на место разрешающего
            left++;            // сдвигаем левую границу вправо
        }
        while ((a[left] <= pivot) && (left < right))
            left++;       // сдвигаем левую границу пока элемент [left] меньше [pivot]
        if (left != right)// если границы не сомкнулись
        {
            a[right] = a[left];// перемещаем элемент [left] на место [right]
            right--;           // сдвигаем правую границу вправо
        }
    }
    a[left] = pivot;// ставим разрешающий элемент на место
    pivot = left;
    left = l_hold;
    right = r_hold;
    if (left < pivot)// Рекурсивно вызываем сортировку для левой и правой части массива
        quickSort(a, left, pivot - 1);
    if (right > pivot)
        quickSort(a, pivot + 1, right);
    return a;
}

// Выписываем столбец матрицы
void *getStolbec(int stolbec) {
    int g = 0;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (stolbec == j) {
                stolbecSort[g] = mass[i][j];
                g++;
            }
        }
    }
}

// Вписываем столбец обратно
void returnStolbec(int stolbec) {
    for (int g = 0; g < n; g++) {
        for (int j = 0; j < m; j++)
            if (stolbec == j) {
                mass[g][j] = stolbecSort[g];
            }
    }
}

// Сортируем по одному столбцу
void quickSortAmmo() {
    for (int stolbec = 0; stolbec < m; stolbec++) {
        getStolbec(stolbec);             // Выписываем нужный столбец
        quickSort(stolbecSort, 0, m - 1);// Сортируем
        returnStolbec(stolbec);
    }
}

int main() {
    // Узнаем размеры
    cout << "Введите строки N: " << endl;
    cin >> n;
    cout << "Введите столбцы M: " << endl;
    cin >> m;
    stolbecSort = new int[m];
    mass = new int *[n];
    for (int i = 0; i < n; i++) {
        mass[i] = new int[m];
        for (int j = 0; j < m; j++)
            mass[i][j] = -9 + rand() % 18;
    }

    // Вывод первичной матрицы
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++)
            cout << mass[i][j] << "   ";
        cout << endl;
    }

    cout << "Подсчет простых чисел в массиве" << endl;
    cout << counterSimpleNumber() << endl;

    cout << "Среднее арифметическое" << endl;
    cout << arithmeticalMean() << endl;

    cout << "Быстрая сортировка по столбцам" << endl;
    quickSortAmmo();
    cout << "Вывод сортированной матрицы" << endl;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++)
            cout << mass[i][j] << "   ";
        cout << endl;
    }
}
